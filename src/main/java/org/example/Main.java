package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://localhost:8080/home.html");

        List<WebElement> inputs = driver.findElements(By.cssSelector("input"));
        Thread.sleep(1000);
        inputs.get(0).sendKeys("2");
        Thread.sleep(1000);
        inputs.get(1).sendKeys("4");
        Thread.sleep(1000);
        WebElement button = driver.findElement(By.cssSelector("button"));
        button.click();

        Thread.sleep(1000);
        List<WebElement> pElements = driver.findElements(By.cssSelector("p"));
        String result = pElements.get(1).getText();
        if(result.equals("6")) {
            System.out.println("merge bine!!!");
        } else {
            System.out.println("am gasit un bug!!!");
        }
    }
}