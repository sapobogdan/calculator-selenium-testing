package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class AltexCrawler {
    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ChromeDriver driver = new ChromeDriver();
                driver.get("https://altex.ro/aer-conditionat-grundig-grepe120-12000-btu-a-a-inverter-wi-fi-kit-instalare-inclus-gri-negru-oglinda/cpd/AERGREPE120/");
                try {
                    //astept sa se incarce pagina
                    Thread.sleep(2000);
                    WebElement priceElement = driver.findElement(By.cssSelector(".Price-int"));
                    String priceValue = priceElement.getText();
                    System.out.println(priceValue);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }
        }, 1000, 10_000);

    }
}